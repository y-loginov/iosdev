//
//  IDAnimal.m
//  ArrayHomeWork
//
//  Created by Admin on 11.02.15.
//  Copyright (c) 2015 JSD. All rights reserved.
//

#import "IDAnimal.h"

@implementation IDAnimal

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.type = @"BAnimal";
        self.family = @"family";
        self.animalClass = @"class";
    }
    return self;
}
- (NSString*) movement {
    return @"\n base move";
}

- (NSString*) description {
    return [NSString stringWithFormat:@"\n type = %@ \n class = %@ \n family = %@", self.type, self.animalClass, self.family];
}

@end
