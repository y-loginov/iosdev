//
//  IDRunner.m
//  ArrayHomeWork
//
//  Created by Admin on 10.02.15.
//  Copyright (c) 2015 JSD. All rights reserved.
//

#import "IDRunner.h"

@implementation IDRunner

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.name = @"C";
        self.height = 2.3f;
        self.weight = 1.6f;
        self.gender = male;
    }
    return self;
}

- (NSString*) movement {
    return [NSString stringWithFormat:@"running"];
}

@end
