//
//  IDVeloman.m
//  ArrayHomeWork
//
//  Created by Admin on 10.02.15.
//  Copyright (c) 2015 JSD. All rights reserved.
//

#import "IDVeloman.h"

@implementation IDVeloman

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.name = @"Z";
        self.height = 1.9f;
        self.weight = 1.0f;
        self.gender = female;
    }
    return self;
}

- (NSString*) movement {
    return [NSString stringWithFormat:@"driving"];
}

@end
