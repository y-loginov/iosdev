//
//  IDHuman.m
//  ArrayHomeWork
//
//  Created by Admin on 10.02.15.
//  Copyright (c) 2015 JSD. All rights reserved.
//

#import "IDHuman.h"

@implementation IDHuman

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.name = @"B";
        self.height = 1.7f;
        self.weight = 8.0f;
        self.gender = male;
        self.className = NSStringFromClass([self superclass]);
    }
    return self;
}

- (NSString*) movement {
    return [NSString stringWithFormat:@"walking"];
}

- (NSString *) description{
    NSString* str;
    
    if (self.gender == male){
        str = @"male";
    } else {
        str = @"female";
    }
    
    NSString* temp = [NSString stringWithFormat:@"name is %@ \n gender is %@ \n height is %f \n weight is %f"
                      ,self.name
                      ,str
                      ,self.height
                      ,self.weight];
    return temp;
}

@end
