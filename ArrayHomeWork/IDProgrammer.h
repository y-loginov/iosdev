//
//  IDProgrammer.h
//  ArrayHomeWork
//
//  Created by Admin on 11.02.15.
//  Copyright (c) 2015 JSD. All rights reserved.
//

#import "IDHuman.h"

@interface IDProgrammer : IDHuman

@property (strong, nonatomic) NSString* language;
@property (strong, nonatomic) NSString* skill;

@end
