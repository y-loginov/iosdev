//
//  IDAppDelegate.m
//  ArrayHomeWork
//
//  Created by Admin on 10.02.15.
//  Copyright (c) 2015 JSD. All rights reserved.
//

#import "IDAppDelegate.h"
#import "IDHuman.h"
#import "IDRunner.h"
#import "IDVeloman.h"
#import "IDSwimer.h"
#import "IDProgrammer.h"
#import "IDAnimal.h"
#import "IDCat.h"
#import "IDSpider.h"

@implementation IDAppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    // Override point for customization after application launch.
    self.window.backgroundColor = [UIColor whiteColor];
    [self.window makeKeyAndVisible];
    
    IDHuman* human = [[IDHuman alloc] init];
    IDVeloman* velo = [[IDVeloman alloc] init];
    IDRunner* run = [[IDRunner alloc] init];
    IDSwimer* swim = [[IDSwimer alloc] init];
    IDProgrammer* prog = [[IDProgrammer alloc] init];
    IDAnimal* animal = [[IDAnimal alloc] init];
    IDCat* cat = [[IDCat alloc] init];
    IDSpider* spider = [[IDSpider alloc] init];
    
    NSArray* arrayHuman = [NSArray arrayWithObjects:human, velo, run, swim, prog, nil];
    NSArray* arrayAnim = [NSArray arrayWithObjects:animal, cat, spider, nil];
    
    NSArray* sortedHuman = [self sortingArr:arrayHuman : @"name"];
    NSArray* sortedAnimals = [self sortingArr:arrayAnim: @"type"];
    
    NSMutableArray* sorted = [[NSMutableArray alloc] init];
    [sorted addObjectsFromArray:sortedHuman];
    [sorted addObjectsFromArray:sortedAnimals];
    
    for (NSObject* obj in sorted) {
        NSLog(@"%@", obj);
    }
    
    return YES;
}

- (NSArray*) sortingArr: (NSArray*) arrayForSort : (NSString*) descriptionName {
    
    NSSortDescriptor* firstDesc = [NSSortDescriptor sortDescriptorWithKey:descriptionName ascending:YES];
    NSArray* arrDesc = [[NSArray alloc] initWithObjects:firstDesc, nil];
    NSArray* temp = [arrayForSort sortedArrayUsingDescriptors:arrDesc];
    return temp;
}

- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

@end
