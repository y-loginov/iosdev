//
//  IDAnimal.h
//  ArrayHomeWork
//
//  Created by Admin on 11.02.15.
//  Copyright (c) 2015 JSD. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface IDAnimal : NSObject

@property (strong, nonatomic) NSString* type;
@property (strong, nonatomic) NSString* animalClass;
@property (strong, nonatomic) NSString* family;

- (NSString*) movement;

@end
