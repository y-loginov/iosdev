//
//  main.m
//  ArrayHomeWork
//
//  Created by Admin on 10.02.15.
//  Copyright (c) 2015 JSD. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "IDAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([IDAppDelegate class]));
    }
}
