//
//  IDProgrammer.m
//  ArrayHomeWork
//
//  Created by Admin on 11.02.15.
//  Copyright (c) 2015 JSD. All rights reserved.
//

#import "IDProgrammer.h"

@implementation IDProgrammer

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.name = @"Y";
        self.height = 6.2f;
        self.weight = 7.1f;
        self.gender = male;
        self.language = @"Objective-C";
        self.skill = @"Newbie";
    }
    return self;
}

- (NSString*) movement {
    return [NSString stringWithFormat:@"base: %@, self:sitting", [super movement]];
}

- (NSString*) description {
    NSString* tempSuper = [super description];
    NSString* tempSelf = [NSString stringWithFormat:@"languane is %@ \n skill is %@", self.language, self.skill];
    return [NSString stringWithFormat:@"%@ \n %@", tempSuper, tempSelf];
}

@end
