//
//  IDSwimer.m
//  ArrayHomeWork
//
//  Created by Admin on 10.02.15.
//  Copyright (c) 2015 JSD. All rights reserved.
//

#import "IDSwimer.h"

@implementation IDSwimer

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.name = @"G";
        self.height = 6.1f;
        self.weight = 1.1f;
        self.gender = female;
    }
    return self;
}

- (NSString*) movement {
    return [NSString stringWithFormat:@"swimming"];
}

@end
