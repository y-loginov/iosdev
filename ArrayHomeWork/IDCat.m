//
//  IDCat.m
//  ArrayHomeWork
//
//  Created by Admin on 11.02.15.
//  Copyright (c) 2015 JSD. All rights reserved.
//

#import "IDCat.h"

@implementation IDCat

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.type = @"Chordata";
        self.family = @"Cats";
        self.animalClass = @"Mammalia";
    }
    return self;
}

- (NSString*) movement {
    return @"\n walking, running, jumping";
}

@end
