//
//  IDSpider.m
//  ArrayHomeWork
//
//  Created by Admin on 11.02.15.
//  Copyright (c) 2015 JSD. All rights reserved.
//

#import "IDSpider.h"

@implementation IDSpider

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.type = @"Arthropoda";
        self.animalClass = @"Spiders";
        self.family = @"Spider";
    }
    return self;
}

- (NSString*) movement {
    return @"\n walking";
}

@end
