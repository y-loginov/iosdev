//
//  IDHuman.h
//  ArrayHomeWork
//
//  Created by Admin on 10.02.15.
//  Copyright (c) 2015 JSD. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface IDHuman : NSObject

typedef enum {
    male,
    female
} IDGender;

@property (strong, nonatomic) NSString* name;
@property (assign, nonatomic) IDGender gender;
@property (assign, nonatomic) CGFloat height;
@property (assign, nonatomic) CGFloat weight;
@property (strong, nonatomic) NSString* className;

- (NSString*) movement;
- (NSString *) description;

@end
